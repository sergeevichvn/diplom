package com.vnazarov.videoplayerwithsubtitle.screen.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.vnazarov.videoplayerwithsubtitle.R;
import com.vnazarov.videoplayerwithsubtitle.screen.fragment.VideoCardListFragment;

import java.io.File;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends FragmentActivity implements MainView{

    private FilePickerDialog filePickerDialog;
    private MainPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mPresenter = new MainPresenter(this);
        initVideoCardListFragment();
        initFilePickerDialog();
    }

    private void initFilePickerDialog(){
        DialogProperties properties = new DialogProperties();
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.selection_type = DialogConfigs.FILE_SELECT;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = new String[]{"3gp", "mp4", "webm", "mkv"};
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        filePickerDialog = new FilePickerDialog(this, properties);
        filePickerDialog.setTitle(getResources().getString(R.string.main_activity_select_video));
        filePickerDialog.setDialogSelectionListener(files -> mPresenter.addVideoFile(files[0]));
    }


    private void initVideoCardListFragment(){
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.main_fragment_container);

        if(fragment == null){
            fragment = new VideoCardListFragment();
            fm.beginTransaction()
                    .add(R.id.main_fragment_container, fragment)
                    .commit();
        }
    }

    @OnClick(R.id.activity_main_add_video)
    public void openVideoChooser(View view){
        filePickerDialog.show();
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

}
