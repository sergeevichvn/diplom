package com.vnazarov.videoplayerwithsubtitle.subtitles;

import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.VideoView;

import com.vnazarov.videoplayerwithsubtitle.subtitles.format.Caption;
import com.vnazarov.videoplayerwithsubtitle.subtitles.format.TimedTextObject;
import com.vnazarov.videoplayerwithsubtitle.subtitles.util.ConcurrencyUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class SubtitleSeeker {
    
    //How often should we sync the subtitles with the VideoView? (In milliseconds)
    //Decreasing this number will result in smoother subtitles but a slower app
    int delay = 250;

    int prevTime = 0;
    VideoView view = null;
    TimedTextObject textObject;
    TextView subView = null;
    Timer timer;
    ArrayList<Caption> captionList = new ArrayList<>();
    ViewGroup layout;

    /**
     * Construct a subtitle seeker
     * @param view The video view to sync with
     * @param subView The text view to display subtitles on
     */
    public SubtitleSeeker(VideoView view, TextView subView) {
        this.view = view;
        this.subView = subView;
    }


    public SubtitleSeeker(VideoView view, ViewGroup layout) {
        this.view = view;
        this.layout = layout;
    }

    /**
     * Start syncing a subtitle object with the VideoView
     * @param textObject The subtitle to load
     */
    public void sync(TimedTextObject textObject) {
        this.textObject = textObject;

        //Create caption list
        captionList.addAll(textObject.captions.values());
        //Schedule sync task
        timer = new Timer();
        timer.schedule(new SyncTask(), 0, delay);
    }

    /**
     * Stop syncing the subtitle object with the VideoView
     */
    public void deSync() {
        //Cancel display task
        if(timer != null) {
            timer.cancel();
        }
        this.prevTime = 0;
        this.textObject = null;
        this.captionList.clear();
    }

    /**
     * Update the TextView with new captions.
     * @param text The new text to set the TextView to
     */
    void updateTextView(final String text) {
        //setText() is a slow procedure, run it only if we need to
        if (!subView.getText().equals(text)) {
            ConcurrencyUtils.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    subView.setText(text);
                }
            });
        }
    }

    /**
     * Async task to keep the TextView and the VideoView in sync
     */
    public class SyncTask extends TimerTask {

        @Override
        public void run() {
            if (captionList != null && view != null) {
                //Get the current position VideoView
                int currentPosition = view.getCurrentPosition();
                //Check if the user is seeking around
                if(currentPosition < prevTime) {
                    //Re-add all the removed subtitles
                    captionList.clear();
                    captionList.addAll(textObject.captions.values());
                }
                //We may have multiple captions so a StringBuilder is more faster and suitable
                StringBuilder captionBuilder = new StringBuilder();
                //Go through all captions to display
                Iterator<Caption> captionIterator = captionList.iterator();
                while (captionIterator.hasNext()) {
                    Caption caption = captionIterator.next();
                    if (caption.start.getMseconds() <= currentPosition && caption.end.getMseconds() >= currentPosition) {
                        //Append the caption
                        if (captionBuilder.length() > 0) {
                            captionBuilder.append('\n');
                        }
                        captionBuilder.append(caption.content);
                    } else {
                        //Remove captions behind our current position
                        if(caption.start.getMseconds() < currentPosition && caption.end.getMseconds() < currentPosition) {
                            captionIterator.remove();
                        }
                    }
                }
                prevTime = currentPosition;
                final String caption = captionBuilder.toString();
                //Update the textview
                updateTextView(caption);
            }
        }
    }
}
