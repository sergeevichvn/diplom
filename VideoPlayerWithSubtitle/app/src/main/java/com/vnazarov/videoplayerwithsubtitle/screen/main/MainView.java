package com.vnazarov.videoplayerwithsubtitle.screen.main;

/**
 * Created by vnazarov on 11.04.2017.
 */

public interface MainView {
    void showMessage(String msg);
}
