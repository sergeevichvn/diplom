package com.vnazarov.videoplayerwithsubtitle.screen.player;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.vnazarov.videoplayerwithsubtitle.R;
import com.vnazarov.videoplayerwithsubtitle.loader.TranslateLoader;
import com.vnazarov.videoplayerwithsubtitle.screen.export.ExportWordsActivity;
import com.vnazarov.videoplayerwithsubtitle.subtitleUtils.Parser;
import com.vnazarov.videoplayerwithsubtitle.subtitleUtils.SubtitlesSynchronizer;
import com.vnazarov.videoplayerwithsubtitle.subtitleUtils.exceptions.ExtensionNotSupportedException;
import com.vnazarov.videoplayerwithsubtitle.subtitleUtils.exceptions.IncorrectTimeFormatException;
import com.vnazarov.videoplayerwithsubtitle.subtitleUtils.pojo.Subtitle;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PlayerActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<String> {

    private static final String TAG = "PlauerActivity";
    private static final String EXTRA_VIDEO_PATH = "com.vnazarov.videoplayerwithsubtitle.VIDEO_PATH";
    private static final String EXTRA_SUBTITLE_PATH = "com.vnazarov.videoplayerwithsubtitle.SUBTITLE_PATH";
    private static final String EXTRA_LAST_POSITION = "com.vnazarov.videoplayerwithsubtitle.LAST_POSITION";
    private static final String EXTRA_STATUS = "com.vnazarov.videoplayerwithsubtitle.STATUS";
    private static final String EXTRA_WORDS = "com.vnazarov.videoplayerwithsubtitle.WORDS";
    private static final int LOADER_ID = 1;


    private String videoPath;
    private String subtitlePath;
    private int position = 0;
    private boolean status = false;
    private HashMap<String, String> words = new HashMap<>();

    private MediaController mc;
    private String mText;

    @BindView(R.id.activity_player_video_player)
    VideoView mVideoView;
    @BindView(R.id.activity_player_subtitles_text)
    TextView mSubtitleTextView;
    @BindView(R.id.activity_player_subtitles_translate_text)
    TextView mSubtitleTranslateTextView;
    @BindView(R.id.activity_player_progress)
    ProgressBar mProgressBar;

    public static Intent newIntent(Context context, String videoPath, String subtitlePath) {
        Intent intent = new Intent(context, PlayerActivity.class);
        intent.putExtra(EXTRA_VIDEO_PATH, videoPath);
        intent.putExtra(EXTRA_SUBTITLE_PATH, subtitlePath);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        videoPath = intent.getStringExtra(EXTRA_VIDEO_PATH);
        subtitlePath = intent.getStringExtra(EXTRA_SUBTITLE_PATH);

        initPlayer();

        if (savedInstanceState != null) {
            mVideoView.seekTo(savedInstanceState.getInt(EXTRA_LAST_POSITION));
            words = (HashMap) savedInstanceState.getSerializable(EXTRA_WORDS);
            if (savedInstanceState.getBoolean(EXTRA_STATUS))
                mVideoView.start();

        }
    }

    @OnClick(R.id.activity_player_subtitles_text)
    public void translateSubtitle() {
        if (mVideoView.isPlaying())
            mVideoView.pause();
    }

    private void initPlayer() {
        //init videoplayer
        if (mc == null) {
            mc = new MediaController(this);
            mc.setAnchorView(mVideoView);
        }

        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                startActivity(ExportWordsActivity.newIntent(PlayerActivity.this, words));
            }
        });

        mVideoView.setMediaController(mc);
        mVideoView.setVideoPath(videoPath);
        mVideoView.start();

        //init subtitlePath if they exist
        if (subtitlePath == null)
            return;

        SubtitlesSynchronizer ss = new SubtitlesSynchronizer(mVideoView, mSubtitleTextView);
        try {
            List<Subtitle> subtitles = new Parser().parsFile(subtitlePath);
            ss.start(subtitles);
        } catch (ExtensionNotSupportedException e) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
        } catch (IncorrectTimeFormatException e) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
        }

        mSubtitleTextView.setTextIsSelectable(true);
        mSubtitleTextView.setCustomSelectionActionModeCallback(new TranslateActiveMode());
        mSubtitleTextView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mVideoView.isPlaying())
                    mVideoView.pause();
                return false;
            }
        });
    }

    /*
        Предлагаем экспортировать неизвестные слова, если они есть, при попытки выйти из плеера
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (words.size() > 0)
            startActivity(ExportWordsActivity.newIntent(this, words));
    }

    /*
        Сохраняем состояние, если активити не в фофкусе
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(EXTRA_LAST_POSITION, position);
        outState.putBoolean(EXTRA_STATUS, status);
        outState.putSerializable(EXTRA_WORDS, words);
    }

    /*
        Сохраняем состояние, если активити не в фофкусе
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (mVideoView == null)
            return;

        if (mVideoView.isPlaying()) {
            mVideoView.pause();
            status = true;
        }

        position = mVideoView.getCurrentPosition();
    }

    /*
        Восстанавливаем состояние
     */
    @Override
    protected void onResume() {
        super.onResume();
        mVideoView.seekTo(position);
        if(status)
            mVideoView.start();
    }

    /*
        Создаем Loader для запроса перевода с сервера Яндекс
     */
    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new TranslateLoader(this, mText);
    }

    /*
        Получаем перевод
     */
    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        words.put(mText, data);
        mProgressBar.setVisibility(View.INVISIBLE);
        mSubtitleTranslateTextView.setText(data);
        getSupportLoaderManager().destroyLoader(loader.getId());
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {}


    /*
        Класс описывающий контекстное меню для перевода выделенного текста
     */
    public class TranslateActiveMode implements ActionMode.Callback {

        private static final int YANDEX_ID = 1;
        private static final int GOOGLE_ID = 2;

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            menu.add(0, YANDEX_ID, 0, "Yandex");
            menu.add(0, GOOGLE_ID, 0, "Google");

            //Remove other items
            menu.removeItem(android.R.id.selectAll);
            menu.removeItem(android.R.id.copy);
            menu.removeItem(android.R.id.paste);
            menu.removeItem(android.R.id.shareText);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return true;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case YANDEX_ID:
                    mText = getSelectedText();
                    PlayerActivity.this.getSupportLoaderManager().initLoader(LOADER_ID, null, PlayerActivity.this);
                    mProgressBar.setVisibility(View.VISIBLE);
                    break;
                case GOOGLE_ID:

                    break;
            }
            return false;
        }

        private String getSelectedText() {
            int start = mSubtitleTextView.getSelectionStart();
            int end = mSubtitleTextView.getSelectionEnd();
            String text = mSubtitleTextView.getText().subSequence(start, end).toString();
            return text;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {

        }
    }
}
