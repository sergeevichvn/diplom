package com.vnazarov.videoplayerwithsubtitle.screen.fragment;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.vnazarov.videoplayerwithsubtitle.R;
import com.vnazarov.videoplayerwithsubtitle.data.database.DatabaseAccess;
import com.vnazarov.videoplayerwithsubtitle.model.VideoCard;
import com.vnazarov.videoplayerwithsubtitle.screen.player.PlayerActivity;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by vnazarov on 11.04.2017.
 */

public class VideoCardHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.item_videocard_video_path) TextView videoPath;
    @BindView(R.id.item_videocard_subtitle_path) TextView subtitlePath;

    private VideoCard mVideoCard;
    private DialogProperties properties;
    private FilePickerDialog filePickerDialog;
    private Context context;

    public VideoCardHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = itemView.getContext();
    }

    public void bindHolder(VideoCard videoCard) {
        this.mVideoCard = videoCard;
        videoPath.setText("Video: " + mVideoCard.getVideoPath());

        if (videoCard.getSubPath() != null)
            setSubtitleStatus(videoCard.getSubPath());
        else
            setSubtitleStatus("not selected");
    }

    private void initFilePickerDialog(){
        properties = new DialogProperties();
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.selection_type = DialogConfigs.FILE_SELECT;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = new String[]{"srt"};
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        filePickerDialog = new FilePickerDialog(context, properties);
        filePickerDialog.setTitle("SELECT SUBTITLE");
        filePickerDialog.setDialogSelectionListener(new DialogSelectionListener() {
            @Override
            public void onSelectedFilePaths(String[] files) {
                if(files.length == 0) {
                    play();
                    return;
                }

                String path = files[0];
                mVideoCard.setSubPath(path);
                DatabaseAccess.update(mVideoCard);
                setSubtitleStatus(path);
                play();
            }
        });
    }

    @OnClick(R.id.item_videocard_play)
    public void selectSubtitleAndStartVideo(){
        if(mVideoCard.getSubPath() != null){
            play();
        } else {
            initFilePickerDialog();
            filePickerDialog.show();
        }
    }

    @OnClick(R.id.item_videocard_delete)
    public void deleteVideo(){
        DatabaseAccess.delete(mVideoCard);
    }

    public void play() {
        Intent i = PlayerActivity.newIntent(context, mVideoCard.getVideoPath(), mVideoCard.getSubPath());
        context.startActivity(i);
    }

    public void setSubtitleStatus(String path) {
        subtitlePath.setText("Subtitles: " + path);
    }

}
