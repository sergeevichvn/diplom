package com.vnazarov.videoplayerwithsubtitle.subtitleUtils;

import android.util.Log;

import com.vnazarov.videoplayerwithsubtitle.subtitleUtils.exceptions.ExtensionNotSupportedException;
import com.vnazarov.videoplayerwithsubtitle.subtitleUtils.exceptions.IncorrectTimeFormatException;
import com.vnazarov.videoplayerwithsubtitle.subtitleUtils.pojo.Subtitle;
import com.vnazarov.videoplayerwithsubtitle.subtitleUtils.pojo.Time;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vnazarov on 01.03.2017.
 */

public class Parser {

    public List<Subtitle> parsFile(String path) throws ExtensionNotSupportedException, IOException, IncorrectTimeFormatException {
        String extension = getExtesion(path);
        switch (extension) {
            case "srt":
                return parseSRT(path);

            default:
                throw new ExtensionNotSupportedException(extension);
        }
    }

    public String getExtesion(String path){
        int start = path.lastIndexOf(".")+1;
        return path.substring(start);
    }

    public BufferedReader getBufferedReader(String path) throws FileNotFoundException {
        File subtitles = new File(path);
        InputStream is = new FileInputStream(subtitles);
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        return br;
    }

    public List<Subtitle> parseSRT(String path) throws IOException, IncorrectTimeFormatException {
        final String BOM = "\uFEFF";
        BufferedReader br = getBufferedReader(path);
        List<Subtitle> list = new ArrayList<>();
        String line = null;

        do{
            //line = br.readLine().trim().replace(BOM, "");
            line = br.readLine();

            //является ли line числом
            if(!line.matches("^[0-9]+$") && line != null)
                continue;

            Subtitle subtitle = new Subtitle();
            line = br.readLine().trim();
            Log.d("parseSRT", line);
            String start = line.substring(0, 12);
            String end = line.substring(line.length()-12);
            subtitle.setStart(new Time("hh:mm:ss,ms", start));
            subtitle.setEnd(new Time("hh:mm:ss,ms", end));
            String text = "";
            line = br.readLine();
            while(line != null && !line.isEmpty()){
                line = line.trim();
                text += deleteXMLTags(line)+"\n";
                line = br.readLine();
            }
            subtitle.setText(text);
            list.add(subtitle);
        }while (line != null);

        br.close();

        return list;
    }

    public String deleteXMLTags(String line){
        return line.replaceAll("\\<.*?\\>", "");
    }
}
