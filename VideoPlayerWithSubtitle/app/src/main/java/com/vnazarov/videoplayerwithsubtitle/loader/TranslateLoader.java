package com.vnazarov.videoplayerwithsubtitle.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.vnazarov.videoplayerwithsubtitle.model.YandexResponse;
import com.vnazarov.videoplayerwithsubtitle.network.ApiFactory;
import com.vnazarov.videoplayerwithsubtitle.network.YandexTranslateService;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by vnazarov on 30.03.2017.
 */

public class TranslateLoader extends AsyncTaskLoader<String> {

    private final String TAG = getClass().getName();
    private static final String API_KEY = "trnsl.1.1.20170123T112431Z.05fb5d4e1a93f3b2.d19ff74f0e3deeea7d5ce239e061e186812abf29";
    private static final String LANG = "en-ru";
    private static String mText;

    public TranslateLoader(Context context, String text) {
        super(context);
        mText = text;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public String loadInBackground() {
        try {
            return ApiFactory.getYandexTranslateService().getTranslate(API_KEY, mText, LANG).execute().body().getFirstTranslate();
        } catch (IOException e) {
            return "Ошибка: " + e.toString();
        }
    }

    /*
    * Пришел ответ
    * */
    @Override
    public void deliverResult(String data) {
        super.deliverResult(data);
    }
}
