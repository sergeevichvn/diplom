package com.vnazarov.videoplayerwithsubtitle.screen.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vnazarov.videoplayerwithsubtitle.R;
import com.vnazarov.videoplayerwithsubtitle.model.VideoCard;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class VideoCardListFragment extends Fragment implements VideoCardListFragmentView{
    @BindView(R.id.video_card_list_recycle_view) RecyclerView mRecycleView;

    private Unbinder mUnbinder;
    private VideoCardListFragmentPresenter mPresenter;


    public VideoCardListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_card_list, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        mRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mPresenter = new VideoCardListFragmentPresenter(this);
        mPresenter.init();
        mPresenter.registerObserver();

        return view;
    }

    public void updateUI() {
        List<VideoCard> videoCards = mPresenter.getAllVideo();
        VideoCardAdapter mVideoCardAdapter = new VideoCardAdapter(videoCards);
        mRecycleView.setAdapter(mVideoCardAdapter);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.unregisterObserver();
    }
}
