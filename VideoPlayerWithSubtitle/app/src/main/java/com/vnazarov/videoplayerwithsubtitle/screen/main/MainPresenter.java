package com.vnazarov.videoplayerwithsubtitle.screen.main;

import com.vnazarov.videoplayerwithsubtitle.data.database.DatabaseAccess;
import com.vnazarov.videoplayerwithsubtitle.model.VideoCard;

/**
 * Created by vnazarov on 11.04.2017.
 */

public class MainPresenter {

    private final MainView mView;
    private static final String ADD = "Add";
    private static final String ALREADY_EXIST = "Video already exist";


    public MainPresenter(MainView mView) {
        this.mView = mView;
    }

    private String getNameFromPath(String path){
        int start = path.lastIndexOf("/")+1;
        int end = path.lastIndexOf(".");
        return path.substring(start, end);
    }

    public void addVideoFile(String path){
        String name = getNameFromPath(path);
        VideoCard video = new VideoCard();
        video.setVideoPath(path);
        video.setName(name);

        if(!DatabaseAccess.checkExist(video)){
            DatabaseAccess.add(video);
            mView.showMessage(ADD + path); //Вынести в string res
        } else {
            mView.showMessage(ALREADY_EXIST);
        }
    }
}
