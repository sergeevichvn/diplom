package com.vnazarov.videoplayerwithsubtitle.data.database.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.vnazarov.videoplayerwithsubtitle.model.VideoCard;

import org.sqlite.database.sqlite.SQLiteDatabase;

import ru.arturvasilov.sqlite.core.BaseTable;
import ru.arturvasilov.sqlite.core.Table;
import ru.arturvasilov.sqlite.utils.TableBuilder;

/**
 * Created by vnazarov on 03.04.2017.
 */

public class VideoCardTable extends BaseTable<VideoCard> {

    public static final Table<VideoCard> TABLE = new VideoCardTable();

    public static final String ENTITY_ID = "entity_id";
    public static final String VIDEO_PATH_COLUMN = "video";
    public static final String SUB_PATH_COLUMN = "sub";
    public static final String POSITION_COLUMN = "position";
    public static final String NAME_VIDEO = "name";

    @Override
    public void onCreate(@NonNull SQLiteDatabase database) {
        TableBuilder.create(this)
                .primaryKey(ENTITY_ID)
                .intColumn(ENTITY_ID)
                .textColumn(NAME_VIDEO)
                .textColumn(VIDEO_PATH_COLUMN)
                .textColumn(SUB_PATH_COLUMN)
                .intColumn(POSITION_COLUMN)
                .execute(database);
    }

    @NonNull
    @Override
    public ContentValues toValues(@NonNull VideoCard videoCard) {
        ContentValues values = new ContentValues();
        values.put(ENTITY_ID, videoCard.getId());
        values.put(NAME_VIDEO, videoCard.getName());
        values.put(VIDEO_PATH_COLUMN, videoCard.getVideoPath());
        values.put(SUB_PATH_COLUMN, videoCard.getSubPath());
        values.put(POSITION_COLUMN, videoCard.getPosition());
        return values;
    }

    @NonNull
    @Override
    public VideoCard fromCursor(@NonNull Cursor cursor) {
        VideoCard videoCard = new VideoCard();
        videoCard.setId(cursor.getInt(cursor.getColumnIndex(ENTITY_ID)));
        videoCard.setName(cursor.getString(cursor.getColumnIndex(NAME_VIDEO)));
        videoCard.setVideoPath(cursor.getString(cursor.getColumnIndex(VIDEO_PATH_COLUMN)));
        videoCard.setSubPath(cursor.getString(cursor.getColumnIndex(SUB_PATH_COLUMN)));
        videoCard.setPosition(cursor.getInt(cursor.getColumnIndex(POSITION_COLUMN)));
        return videoCard;
    }
}
