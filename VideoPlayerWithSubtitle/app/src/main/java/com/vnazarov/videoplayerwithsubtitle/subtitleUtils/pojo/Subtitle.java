package com.vnazarov.videoplayerwithsubtitle.subtitleUtils.pojo;

public class Subtitle {

    private Time start;
    private Time end;
    private String text;


    public Time getStart() {
        return start;
    }

    public void setStart(Time start) {
        this.start = start;
    }

    public Time getEnd() {
        return end;
    }

    public void setEnd(Time end) {
        this.end = end;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
