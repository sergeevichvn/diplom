package com.vnazarov.videoplayerwithsubtitle.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by vnazarov on 05.04.2017.
 */

public final class Utils {

    public static String getNameFromPath(String path){
        int start = path.lastIndexOf("/")+1;
        int end = path.lastIndexOf(".");
        return path.substring(start, end);
    }

    public static void showMessage(Context context, String message){
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
