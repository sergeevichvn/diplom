package com.vnazarov.videoplayerwithsubtitle.screen.fragment;

import com.vnazarov.videoplayerwithsubtitle.data.database.DatabaseAccess;
import com.vnazarov.videoplayerwithsubtitle.model.VideoCard;

import java.util.List;

import ru.arturvasilov.sqlite.core.BasicTableObserver;

/**
 * Created by vnazarov on 11.04.2017.
 */

public class VideoCardListFragmentPresenter implements BasicTableObserver {

    private final VideoCardListFragmentView mView;

    public VideoCardListFragmentPresenter(VideoCardListFragmentView mView) {
        this.mView = mView;
    }

    public void init(){
        mView.updateUI();
    }

    public List<VideoCard> getAllVideo(){
        return DatabaseAccess.getAll();
    }

    public void deleteVideo(VideoCard videoCard){
        DatabaseAccess.delete(videoCard);
        mView.updateUI();
    }

    @Override
    public void onTableChanged() {
        mView.updateUI();
    }

    public void registerObserver(){
        DatabaseAccess.registerObserver(this);
    }

    public void unregisterObserver(){
        DatabaseAccess.unregisterObserver(this);
    }
}
