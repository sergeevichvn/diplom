package com.vnazarov.videoplayerwithsubtitle.data.database;

import com.vnazarov.videoplayerwithsubtitle.data.database.tables.VideoCardTable;
import com.vnazarov.videoplayerwithsubtitle.model.VideoCard;
import java.util.List;

import ru.arturvasilov.sqlite.core.BasicTableObserver;
import ru.arturvasilov.sqlite.core.SQLite;
import ru.arturvasilov.sqlite.core.Where;

public class DatabaseAccess {

    public static VideoCard getById(Integer id) {
        return SQLite.get().query(VideoCardTable.TABLE, Where.create().equalTo(VideoCardTable.ENTITY_ID, id)).get(0);
    }

    public static void add(VideoCard entity) {
        SQLite.get().insert(VideoCardTable.TABLE, entity);
        videoCardTableWasChanged();
    }

    public static void delete(VideoCard entity) {
        SQLite.get().delete(VideoCardTable.TABLE, Where.create().equalTo(VideoCardTable.VIDEO_PATH_COLUMN, entity.getVideoPath()));
        videoCardTableWasChanged();
    }

    public static List<VideoCard> getAll() {
        return SQLite.get().query(VideoCardTable.TABLE);
    }


    public static void update(VideoCard entity){
        SQLite.get().update(VideoCardTable.TABLE, Where.create().equalTo(VideoCardTable.VIDEO_PATH_COLUMN, entity.getVideoPath()), entity);
        videoCardTableWasChanged();
    }

    public static boolean checkExist(VideoCard videoCard){
        return SQLite.get().query(VideoCardTable.TABLE, Where.create().equalTo(VideoCardTable.VIDEO_PATH_COLUMN, videoCard.getVideoPath())).size() > 0 ? true: false;
    }

    public static void registerObserver(BasicTableObserver observer){
        SQLite.get().registerObserver(VideoCardTable.TABLE, observer);
    }

    //Pattern A
    public static void unregisterObserver(BasicTableObserver observer){
        SQLite.get().unregisterObserver(observer);
    }

    private static void videoCardTableWasChanged(){
        SQLite.get().notifyTableChanged(VideoCardTable.TABLE);
    }
}