package com.vnazarov.videoplayerwithsubtitle.subtitleUtils.exceptions;

/**
 * Created by vnazarov on 09.03.2017.
 */

public class IncorrectTimeFormatException extends Exception {

    public IncorrectTimeFormatException() {
        super("Incorrect time format! Must be - hh:mm:ss,ms");
    }
}
