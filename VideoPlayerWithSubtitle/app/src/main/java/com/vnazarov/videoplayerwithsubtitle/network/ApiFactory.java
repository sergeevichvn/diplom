package com.vnazarov.videoplayerwithsubtitle.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by vnazarov on 30.03.2017.
 */

public class ApiFactory {

    private static final String SITE = "https://translate.yandex.net/";
    private static volatile YandexTranslateService mService;

    public static YandexTranslateService getYandexTranslateService(){
        YandexTranslateService service = mService;
        if (service == null) {
            synchronized (ApiFactory.class) {
                service = mService;
                if (service == null) {
                    service = mService = buildRetrofit().create(YandexTranslateService.class);
                }
            }
        }
        return service;
    }


    private static Retrofit buildRetrofit(){
        return new Retrofit.Builder()
                .baseUrl(SITE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
