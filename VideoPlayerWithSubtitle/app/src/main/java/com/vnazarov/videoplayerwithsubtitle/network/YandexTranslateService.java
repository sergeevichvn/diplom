package com.vnazarov.videoplayerwithsubtitle.network;

import com.vnazarov.videoplayerwithsubtitle.model.YandexResponse;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by vnazarov on 23.01.2017.
 */

public interface YandexTranslateService {
    @POST("api/v1.5/tr.json/translate")
    Call<YandexResponse> getTranslate(@Query("key") String key, @Query("text") String text, @Query("lang") String lang);
}
