package com.vnazarov.videoplayerwithsubtitle.subtitleUtils;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.TextView;
import android.widget.VideoView;

import com.vnazarov.videoplayerwithsubtitle.subtitleUtils.pojo.Subtitle;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by vnazarov on 01.03.2017.
 */

public class SubtitlesSynchronizer {

    private VideoView videoView;
    private TextView textView;
    private List<Subtitle> subtitles;
    private static Timer task;

    public SubtitlesSynchronizer(VideoView videoView, TextView viewGroup) {
        this.videoView = videoView;
        this.textView = viewGroup;
    }

    public void start(List<Subtitle> subtitles){
        this.subtitles = subtitles;
        if(task == null)
            task = new Timer();
        task.schedule(new SyncTask(), 0, 250);
    }

    public void stop(){
        if(task != null){
            task.cancel();
        }
    }

    private void updateTextView(final String text){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                textView.setText(text);
            }
        });
    }

    private class SyncTask extends TimerTask{
        @Override
        public void run() {
            if(!videoView.isPlaying())
                return;

            int currentPosition = videoView.getCurrentPosition();
            for(Subtitle subtitle: subtitles){
                if(subtitle.getStart().getMseconds() <= currentPosition && subtitle.getEnd().getMseconds() >= currentPosition) {
                    //if(!textView.getText().toString().equals(subtitle.getText())) {
                        Log.d("SyncTask", "UPDATETEXT");
                        updateTextView(subtitle.getText());
                    //}
                }
            }

        }
    }
}
