package com.vnazarov.videoplayerwithsubtitle;

import android.app.Application;

import ru.arturvasilov.sqlite.core.SQLite;

/**
 * Created by vnazarov on 03.04.2017.
 */

public class AppDelegate extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SQLite.initialize(this);
    }
}
