package com.vnazarov.videoplayerwithsubtitle.data.database.sqlite;

import android.support.annotation.NonNull;

import com.vnazarov.videoplayerwithsubtitle.data.database.tables.VideoCardTable;

import ru.arturvasilov.sqlite.core.SQLiteConfig;
import ru.arturvasilov.sqlite.core.SQLiteContentProvider;
import ru.arturvasilov.sqlite.core.SQLiteSchema;

/**
 * Created by vnazarov on 03.04.2017.
 */

public class DatabaseContentProvider extends SQLiteContentProvider {

    private static final String DATABASE_NAME = "com.vnazarov.videoplayerwithsubtitle.database";
    private static final String CONTENT_AUTHORITY = "com.vnazarov.videoplayerwithsubtitle";

    @Override
    protected void prepareConfig(@NonNull SQLiteConfig config) {
        config.setDatabaseName(DATABASE_NAME);
        config.setAuthority(CONTENT_AUTHORITY);
    }

    @Override
    protected void prepareSchema(@NonNull SQLiteSchema schema) {
        schema.register(VideoCardTable.TABLE);
    }
}
