package com.vnazarov.videoplayerwithsubtitle.subtitleUtils.exceptions;

/**
 * Created by vnazarov on 01.03.2017.
 */

public class ExtensionNotSupportedException extends Exception {

    public ExtensionNotSupportedException(String extension) {
        super("Extension " + extension + " not supported");
    }
}
