package com.vnazarov.videoplayerwithsubtitle.screen.export;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.ShareCompat;

import com.vnazarov.videoplayerwithsubtitle.utils.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import static android.R.attr.data;

/**
 * Created by vnazarov on 11.04.2017.
 */

public class ExportWordsPresenter {

    private final ExportWordsView mView;

    public ExportWordsPresenter(ExportWordsView mView) {
        this.mView = mView;
    }

    public void exportToFile(HashMap<String, String> words){
        final String ROOT_FOLDER = "words";
        File root = new File(Environment.getExternalStorageDirectory(), ROOT_FOLDER);
        if(!root.exists())
            root.mkdirs();

        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
        String filename = "words_"+ sdf.format(Calendar.getInstance().getTime())+".html";
        File wordsFile = new File(root, filename);
        try {
            wordsFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(wordsFile);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fos);
            outputStreamWriter.write("<html><head><meta charset=\"utf-8\"/></head><body>");
            for(String key: words.keySet()){
                outputStreamWriter.write(key + " - " + words.get(key) + ";\n");
            }
            outputStreamWriter.write("</body></html>");
            outputStreamWriter.close();
            Utils.showMessage(mView.getActivityContext(), "Save to file: words/" + filename);
        } catch (IOException e) {
            Utils.showMessage(mView.getActivityContext(), "Error write to file words/" + filename + "\n" + e.toString());
        }
    }

    public void exportToAnki(HashMap<String, String> words){

    }
}
