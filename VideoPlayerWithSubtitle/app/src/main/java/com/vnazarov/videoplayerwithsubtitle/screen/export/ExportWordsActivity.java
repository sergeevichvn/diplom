package com.vnazarov.videoplayerwithsubtitle.screen.export;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ShareCompat;
import android.util.SparseBooleanArray;
import android.widget.Button;
import android.widget.Toast;

import com.vnazarov.videoplayerwithsubtitle.R;
import com.vnazarov.videoplayerwithsubtitle.anki.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ExportWordsActivity extends Activity implements ExportWordsView{

    private static final String EXTRA_WORDS = "com.vnazarov.videoplayerwithsubtitle.WORDS";
    private static final int AD_PERM_REQUEST = 0;

    private HashMap<String, String> words;
    private ExportWordsPresenter mPresenter;
    private AnkiDroidHelper mAnkiDroid;
    List<Map<String, String>> mListData;

    @BindView(R.id.file_export_button)
    Button toFileButton;
    @BindView(R.id.anki_export_button)
    Button toAnkiButton;



    public static Intent newIntent(Context context, HashMap<String, String> words){
        Intent intent = new Intent(context, ExportWordsActivity.class);
        intent.putExtra(EXTRA_WORDS, words);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_export_words);
        ButterKnife.bind(this);
        mPresenter = new ExportWordsPresenter(this);

        Intent intent = getIntent();
        words = (HashMap) intent.getSerializableExtra(EXTRA_WORDS);
        mListData = new ArrayList<>();
        mListData.add(words);
        mAnkiDroid = new AnkiDroidHelper(this);
    }

    @OnClick(R.id.file_export_button)
    public void exportToFile(){
        mPresenter.exportToFile(words);
    }

    @OnClick(R.id.anki_export_button)
    public void exportToAnki(){
        if (mAnkiDroid.shouldRequestPermission()) {
            mAnkiDroid.requestPermission(ExportWordsActivity.this, AD_PERM_REQUEST);
            return;
        }
        addCardsToAnkiDroid(mListData);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode==AD_PERM_REQUEST && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            addCardsToAnkiDroid(mListData);
        } else {
            Toast.makeText(ExportWordsActivity.this, "Couldn\'t get permission to access the AnkiDroid database", Toast.LENGTH_LONG).show();
        }
    }

    private long getModelId() {
        Long mid = mAnkiDroid.findModelIdByName(AnkiDroidConfig.MODEL_NAME, AnkiDroidConfig.FIELDS.length);
        if (mid == null) {
            mid = mAnkiDroid.getApi().addNewCustomModel(AnkiDroidConfig.MODEL_NAME, AnkiDroidConfig.FIELDS,
                    AnkiDroidConfig.CARD_NAMES, AnkiDroidConfig.QFMT, AnkiDroidConfig.AFMT, AnkiDroidConfig.CSS, getDeckId(), null);
            mAnkiDroid.storeModelReference(AnkiDroidConfig.MODEL_NAME, mid);
        }
        return mid;
    }

    private void addCardsToAnkiDroid(final List<Map<String, String>> data) {
        long deckId =getDeckId();
        long modelId = getModelId();
        String[] fieldNames = mAnkiDroid.getApi().getFieldList(modelId);
        // Build list of fields and tags
        LinkedList<String []> fields = new LinkedList<>();
        LinkedList<Set<String>> tags = new LinkedList<>();
        for (Map<String, String> fieldMap: data) {
            for(String word: fieldMap.keySet()){
                String[] flds = new String[fieldNames.length];
                flds[0] = word;
                flds[1] = "";
                flds[2] = fieldMap.get(word);
                flds[3] = "";
                flds[4] = "";
                flds[5] = "";
                flds[6] = "";
                flds[7] = "";
                tags.add(AnkiDroidConfig.TAGS);
                fields.add(flds);
            }
        }
        // Remove any duplicates from the LinkedLists and then add over the API
        mAnkiDroid.removeDuplicates(fields, tags, modelId);
        int added = mAnkiDroid.getApi().addNotes(modelId, deckId, fields, tags);
        Toast.makeText(ExportWordsActivity.this, "Words added", Toast.LENGTH_LONG).show();
    }

    private long getDeckId() {
        Long did = mAnkiDroid.findDeckIdByName(AnkiDroidConfig.DECK_NAME);
        if (did == null) {
            did = mAnkiDroid.getApi().addNewDeck(AnkiDroidConfig.DECK_NAME);
            mAnkiDroid.storeDeckReference(AnkiDroidConfig.DECK_NAME, did);
        }
        return did;
    }

    List<Map<String, String>> getSelectedData() {
        return mListData;
    }


    @Override
    public Context getActivityContext() {
        return this.getApplicationContext();
    }
}
