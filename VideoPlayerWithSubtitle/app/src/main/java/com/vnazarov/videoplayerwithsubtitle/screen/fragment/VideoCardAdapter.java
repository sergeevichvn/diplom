package com.vnazarov.videoplayerwithsubtitle.screen.fragment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vnazarov.videoplayerwithsubtitle.R;
import com.vnazarov.videoplayerwithsubtitle.model.VideoCard;

import java.util.List;

/**
 * Created by vnazarov on 12.04.2017.
 */

public class VideoCardAdapter extends RecyclerView.Adapter<VideoCardHolder> {

    private List<VideoCard> mVideoCards;

    public VideoCardAdapter(List<VideoCard> mVideoCards) {
        this.mVideoCards = mVideoCards;
    }

    @Override
    public VideoCardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View item = layoutInflater.inflate(R.layout.item_videocard, parent, false);
        return new VideoCardHolder(item);
    }

    @Override
    public void onBindViewHolder(VideoCardHolder holder, int position) {
        VideoCard videoCard = mVideoCards.get(position);
        holder.bindHolder(videoCard);
    }

    @Override
    public int getItemCount() {
        return mVideoCards.size();
    }
}
