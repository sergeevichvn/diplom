package com.vnazarov.videoplayerwithsubtitle.screen.export;

import android.content.Context;

/**
 * Created by vnazarov on 11.04.2017.
 */

public interface ExportWordsView {

    Context getActivityContext();
}
