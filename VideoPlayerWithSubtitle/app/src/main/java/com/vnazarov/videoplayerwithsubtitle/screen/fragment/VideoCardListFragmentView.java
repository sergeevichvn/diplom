package com.vnazarov.videoplayerwithsubtitle.screen.fragment;

/**
 * Created by vnazarov on 11.04.2017.
 */

public interface VideoCardListFragmentView {
    void updateUI();
}
