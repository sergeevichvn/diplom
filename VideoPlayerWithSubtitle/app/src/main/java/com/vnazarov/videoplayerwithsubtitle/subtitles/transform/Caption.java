package com.vnazarov.videoplayerwithsubtitle.subtitles.transform;

import com.vnazarov.videoplayerwithsubtitle.subtitleUtils.pojo.Time;

/**
 * Created by vnazarov on 25.01.2017.
 */

public class Caption {

    private Time start;
    private Time end;
    private String caption;

    public Time getStart() {
        return start;
    }

    public void setStart(Time start) {
        this.start = start;
    }

    public Time getEnd() {
        return end;
    }

    public void setEnd(Time end) {
        this.end = end;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Override
    public String toString() {
        return "Caption{" +
                "start=" + start +
                ", end=" + end +
                ", caption='" + caption + '\'' +
                '}';
    }
}
