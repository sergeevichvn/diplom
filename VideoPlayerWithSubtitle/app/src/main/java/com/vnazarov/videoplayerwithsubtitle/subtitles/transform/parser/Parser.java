package com.vnazarov.videoplayerwithsubtitle.subtitles.transform.parser;

import com.vnazarov.videoplayerwithsubtitle.subtitles.transform.Caption;

import java.util.List;

/**
 * Created by vnazarov on 25.01.2017.
 */

public interface Parser {
    public List<Caption> parseFile(String path);
}
